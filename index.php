<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>PHP-KT-Activity 4</title>
</head>
<body style="padding: 10%;">
	<div style="border: 1px solid black; padding: 5%; margin: 10%;">
		<h1>Building</h1>
		<p>The name of the building is <?= $building->getName(); ?>.</p>
		<p><?= $building->getName(); ?> has <?= $building->getFloors(); ?> .</p>
		<p><?= $building->getName(); ?> is located at <?= $building->getAddress(); ?>.</p>

		<p>
		<?php $building->setName("Caswynn Complex"); ?>
		</p>

		<p>The name of the building has been changed to <?= $building->getName();?>.</p>


		<h1>Condominium</h1>
		<p>The name of the condominium is <?= $condominium->getName(); ?>.</p>
		<p><?= $condominium->getName(); ?> has <?= $condominium->getFloors(); ?> .</p>
		<p><?= $condominium->getName(); ?> is located at <?= $condominium->getAddress(); ?>.</p>

		<p>
		<?php $condominium->setName("Enzo Tower"); ?>
		</p>

		<p>The name of the condominium has been changed to <?= $condominium->getName();?>.</p>
	</div>
	
</body>
</html>